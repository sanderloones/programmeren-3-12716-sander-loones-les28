﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Webwinkel.Models
{
    public class Order
    {
        // fields
        protected DateTime orderDate;
        protected DateTime shippingDate;
        protected String comment;
        protected Int32 idShippingMethod;
        protected Int32 idCustomer;
        protected Int32 idOrderStatus;
        protected Int32 id;

        // Getters and setters
        [Display(Name = "BestelDatum")]
        [ScaffoldColumn(true)]
        [Required]
        public DateTime OrderDate
        {
            get { return this.orderDate; }
            set { this.orderDate = value; }
        }

        [Display(Name = "Verzenddatum")]
        [ScaffoldColumn(true)]
        [Required]
        public DateTime ShippingDate
        {
            get { return this.shippingDate; }
            set { this.shippingDate = value; }
        }

        [Display(Name = "Opmerking")]
        [ScaffoldColumn(true)]
        [MaxLength(512, ErrorMessage = "Opmerking bestaat uit maximum 512 karakters.")]
        public String Comment
        {
            get { return this.comment; }
            set { this.comment = value; }
        }

        [ScaffoldColumn(true)]
        [Display(Name = "Kies een vezendmethode")]
        public int ShippingMethodsId
        {
            get { return this.idShippingMethod; }
            set { this.idShippingMethod = value; }
        }

        [ScaffoldColumn(true)]
        [Display(Name = "Kies een klant")]
        public int CustomersId
        {
            get { return this.idCustomer; }
            set { this.idCustomer = value; }
        }

        [ScaffoldColumn(true)]
        [Display(Name = "Kies een order status")]
        public int OrderStatusesId
        {
            get { return this.idOrderStatus; }
            set { this.idOrderStatus = value; }
        }

        [Key]
        [ScaffoldColumn(false)]
        [Required]
        public Int32 Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public virtual ShippingMethod ShippingMethods { get; set; }
        public virtual Customer Customers { get; set; }
        public virtual OrderStatus OrderStatuses { get; set; }
    }
}