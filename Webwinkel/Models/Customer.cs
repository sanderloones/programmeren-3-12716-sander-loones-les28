﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Webwinkel.Models
{
    public class Customer
    {
        // fields
        protected String nickName;
        protected String firstName;
        protected String lastName;
        protected String address1;
        protected String address2;
        protected String city;
        protected String region;
        protected String postalCode;
        protected Int32 idCountry;
        protected String phone;
        protected String mobile;
        protected Int32 id;

        // Getters and setters
        [Display(Name = "Roepnaam")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(10, ErrorMessage = "Roepnaam bestaat uit maximum 10 karakters.")]
        public String NickName
        {
            get { return this.nickName; }
            set { this.nickName = value; }
        }

        [Display(Name = "Voornaam")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(255, ErrorMessage = "Voornaam bestaat uit maximum 255 karakters.")]
        public String FirstName
        {
            get { return this.firstName; }
            set { this.firstName = value; }
        }

        [Display(Name = "Familienaam")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(255, ErrorMessage = "Familienaam bestaat uit maximum 255 karakters.")]
        public String LastName
        {
            get { return this.lastName; }
            set { this.lastName = value; }
        }

        [Display(Name = "Adres 1")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(255, ErrorMessage = "Addres 1 bestaat uit maximum 255 karakters.")]
        public String Address1
        {
            get { return this.address1; }
            set { this.address1 = value; }
        }

        [Display(Name = "Adres 2")]
        [ScaffoldColumn(true)]
        [MaxLength(255, ErrorMessage = "Addres 2 bestaat uit maximum 255 karakters.")]
        public String Address2
        {
            get { return this.address2; }
            set { this.address2 = value; }
        }

        [Display(Name = "Stad")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(255, ErrorMessage = "Stad bestaat uit maximum 255 karakters.")]
        public String City
        {
            get { return this.city; }
            set { this.city = value; }
        }

        [Display(Name = "Regio")]
        [ScaffoldColumn(true)]
        [MaxLength(80, ErrorMessage = "Regio bestaat uit maximum 80 karakters.")]
        public String Region
        {
            get { return this.region; }
            set { this.region = value; }
        }

        [ScaffoldColumn(true)]
        [Display(Name = "Kies een land")]
        public int CountriesId
        {
            get { return this.idCountry; }
            set { this.idCountry = value; }
        }

        [Display(Name = "Postcode")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(20, ErrorMessage = "Postcode bestaat uit maximum 20 karakters.")]
        public String PostalCode
        {
            get { return this.postalCode; }
            set { this.postalCode = value; }
        }

        [Display(Name = "Vaste telefoon")]
        [ScaffoldColumn(true)]
        [MaxLength(40, ErrorMessage = "Phone bestaat uit maximum 40 karakters.")]
        public String Phone
        {
            get { return this.phone; }
            set { this.phone = value; }
        }

        [Display(Name = "Mobieltje")]
        [ScaffoldColumn(true)]
        [MaxLength(40, ErrorMessage = "Mobile bestaat uit maximum 40 karakters.")]
        public String Mobile
        {
            get { return this.mobile; }
            set { this.mobile = value; }
        }

        [Key]
        [ScaffoldColumn(false)]
        [Required]
        public Int32 Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public virtual Country Countries { get; set; }
    }
}