﻿
using System.Data.Entity;

namespace Webwinkel.Models
{
    public class Dal : DbContext
    {
        public Dal() : base("name=WebwinkelWindowsAuthentication")
        { }
        public virtual DbSet<UnitBase> DbSetUnitBase { get; set; }
        public virtual DbSet<Country> DbSetCountry { get; set; }
        public virtual DbSet<Customer> DbSetCustomer { get; set; }
        public virtual DbSet<Supplier> DbSetSupplier { get; set; }
        public virtual DbSet<Product> DbSetProduct { get; set; }
        public virtual DbSet<Category> DbSetCategory { get; set; }
        public virtual DbSet<Order> DbSetOrder { get; set; }
        public virtual DbSet<OrderStatus> DbSetOrderStatus { get; set; }
        public virtual DbSet<ShippingMethod> DbSetShippingMethod { get; set; }

        public System.Data.Entity.DbSet<Webwinkel.Models.OrderItem> OrderItems { get; set; }
    }

}