﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Webwinkel.Models
{
    public class Supplier
    {
        // fields
        protected String code;
        protected String name;
        protected String contact;
        protected String address;
        protected String city;
        protected String region;
        protected String postalCode;
        protected Int32 idCountry;
        protected String phone;
        protected String mobile;
        protected Int32 id;

        // Getters and setters
        [Display(Name = "Code")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(10, ErrorMessage = "Code bestaat uit maximum 10 karakters.")]
        public String Code
        {
            get { return this.code; }
            set { this.code = value; }
        }

        [Display(Name = "Naam")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(255, ErrorMessage = "Naam bestaat uit maximum 255 karakters.")]
        public String Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        [Display(Name = "Contact")]
        [ScaffoldColumn(true)]
        [MaxLength(255, ErrorMessage = "Contact bestaat uit maximum 255 karakters.")]
        public String Contact
        {
            get { return this.contact; }
            set { this.contact = value; }
        }


        [Display(Name = "Adres")]
        [ScaffoldColumn(true)]
        [MaxLength(255, ErrorMessage = "Addres bestaat uit maximum 255 karakters.")]
        public String Address2
        {
            get { return this.address; }
            set { this.address = value; }
        }

        [Display(Name = "Stad")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(255, ErrorMessage = "Stad bestaat uit maximum 255 karakters.")]
        public String City
        {
            get { return this.city; }
            set { this.city = value; }
        }

        [Display(Name = "Regio")]
        [ScaffoldColumn(true)]
        [MaxLength(80, ErrorMessage = "Regio bestaat uit maximum 80 karakters.")]
        public String Region
        {
            get { return this.region; }
            set { this.region = value; }
        }

        [ScaffoldColumn(true)]
        [Display(Name = "Kies een land")]
        public int CountriesId
        {
            get { return this.idCountry; }
            set { this.idCountry = value; }
        }

        [Display(Name = "Postcode")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(20, ErrorMessage = "Postcode bestaat uit maximum 20 karakters.")]
        public String PostalCode
        {
            get { return this.postalCode; }
            set { this.postalCode = value; }
        }

        [Display(Name = "Vaste telefoon")]
        [ScaffoldColumn(true)]
        [MaxLength(40, ErrorMessage = "Phone bestaat uit maximum 40 karakters.")]
        public String Phone
        {
            get { return this.phone; }
            set { this.phone = value; }
        }

        [Display(Name = "Mobieltje")]
        [ScaffoldColumn(true)]
        [MaxLength(40, ErrorMessage = "Mobile bestaat uit maximum 40 karakters.")]
        public String Mobile
        {
            get { return this.mobile; }
            set { this.mobile = value; }
        }

        [Key]
        [ScaffoldColumn(false)]
        [Required]
        public Int32 Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public virtual Country Countries { get; set; }
    }
}