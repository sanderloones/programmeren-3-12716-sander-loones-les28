﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Webwinkel.Models
{
    public class Product
    {
        // fields
        protected String description;
        protected String name;
        protected Double price;
        protected Double shippingCost;
        protected Int32 totalRating;
        protected String thumbnail;
        protected String image;
        protected Double discountPercentage;
        protected Int32 votes;
        protected Int32 idSupplier;
        protected Int32 id;

        // Getters and setters
        [Display(Name = "Beschrijving")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(1024, ErrorMessage = "Beschrijving bestaat uit maximum 1024 karakters.")]
        public String Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        [Display(Name = "Naam")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(255, ErrorMessage = "Naam bestaat uit maximum 255 karakters.")]
        public String Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        [Display(Name = "Prijs")]
        [ScaffoldColumn(true)]
        public Double Price
        {
            get { return this.price; }
            set { this.price = value; }
        }


        [Display(Name = "Verzendkosten")]
        [ScaffoldColumn(true)]
        public Double ShippingCost
        {
            get { return this.shippingCost; }
            set { this.shippingCost = value; }
        }

        [Display(Name = "Totaal rating")]
        [ScaffoldColumn(true)]
        public Int32 TotalRating
        {
            get { return this.totalRating; }
            set { this.totalRating = value; }
        }

        [Display(Name = "Miniatuur")]
        [ScaffoldColumn(true)]
        [MaxLength(255, ErrorMessage = "Miniatuur bestaat uit maximum 255 karakters.")]
        public String Thumbnail
        {
            get { return this.thumbnail; }
            set { this.thumbnail = value; }
        }

        [Display(Name = "Afbeelding")]
        [ScaffoldColumn(true)]
        [MaxLength(255, ErrorMessage = "Miniatuur bestaat uit maximum 255 karakters.")]
        public String Image
        {
            get { return this.image; }
            set { this.image = value; }
        }

        [Display(Name = "Aanbiedingspercent")]
        [ScaffoldColumn(true)]
        public Double DiscountPercentage
        {
            get { return this.discountPercentage; }
            set { this.discountPercentage = value; }
        }

        [Display(Name = "Stemmen")]
        [ScaffoldColumn(true)]
        public Int32 Votes
        {
            get { return this.votes; }
            set { this.votes = value; }
        }

        [ScaffoldColumn(true)]
        [Display(Name = "Kies een supplier")]
        public int SuppliersId
        {
            get { return this.idSupplier; }
            set { this.idSupplier = value; }
        }


        [Key]
        [ScaffoldColumn(false)]
        [Required]
        public Int32 Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public virtual Supplier Suppliers { get; set; }
    }
}