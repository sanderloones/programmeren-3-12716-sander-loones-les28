﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Webwinkel.Models
{
    public class OrderItem
    {
        // fields
     
        protected Int32 quantity;
        protected Int32 idProduct;
        protected Int32 idOrder;
        protected Int32 id;

        // Getters and setters
        [Display(Name = "hoeveelheid")]
        [ScaffoldColumn(true)]
        public Int32 Quantity
        {
            get { return this.quantity; }
            set { this.quantity = value; }
        }

        [ScaffoldColumn(true)]
        [Display(Name = "Kies een product")]
        public int ProductId
        {
            get { return this.idProduct; }
            set { this.idProduct = value; }
        }

        [ScaffoldColumn(true)]
        [Display(Name = "Kies een bestelling")]
        public int OrderId
        {
            get { return this.idOrder; }
            set { this.idOrder = value; }
        }

        [Key]
        [ScaffoldColumn(false)]
        [Required]
        public Int32 Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public virtual ShippingMethod ShippingMethods { get; set; }
        public virtual Customer Customers { get; set; }
        public virtual OrderStatus OrderStatuses { get; set; }
    }
}