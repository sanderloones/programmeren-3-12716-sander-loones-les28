﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Webwinkel.Models;

namespace Webwinkel.Controllers
{
    public class ProductController : Controller
    {
        private Dal db = new Dal();

        // GET: Product
        public ActionResult Index()
        {
            var dbSetProduct = db.DbSetProduct.Include(p => p.Suppliers);
            return View(dbSetProduct.ToList());
        }

        // GET: Product/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.DbSetProduct.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            ViewBag.SuppliersId = new SelectList(db.DbSetSupplier, "Id", "Code");
            return View();
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description,Name,Price,ShippingCost,TotalRating,Thumbnail,Image,DiscountPercentage,Votes,SuppliersId")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.DbSetProduct.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SuppliersId = new SelectList(db.DbSetSupplier, "Id", "Code", product.SuppliersId);
            return View(product);
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.DbSetProduct.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.SuppliersId = new SelectList(db.DbSetSupplier, "Id", "Code", product.SuppliersId);
            return View(product);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description,Name,Price,ShippingCost,TotalRating,Thumbnail,Image,DiscountPercentage,Votes,SuppliersId")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SuppliersId = new SelectList(db.DbSetSupplier, "Id", "Code", product.SuppliersId);
            return View(product);
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.DbSetProduct.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.DbSetProduct.Find(id);
            db.DbSetProduct.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
