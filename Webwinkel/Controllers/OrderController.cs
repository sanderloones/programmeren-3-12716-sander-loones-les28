﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Webwinkel.Models;

namespace Webwinkel.Controllers
{
    public class OrderController : Controller
    {
        private Dal db = new Dal();

        // GET: Order
        public ActionResult Index()
        {
            var dbSetOrder = db.DbSetOrder.Include(o => o.Customers).Include(o => o.OrderStatuses).Include(o => o.ShippingMethods);
            return View(dbSetOrder.ToList());
        }

        // GET: Order/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.DbSetOrder.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Order/Create
        public ActionResult Create()
        {
            ViewBag.CustomersId = new SelectList(db.DbSetCustomer, "Id", "NickName");
            ViewBag.OrderStatusesId = new SelectList(db.DbSetOrderStatus, "Id", "Description");
            ViewBag.ShippingMethodsId = new SelectList(db.DbSetShippingMethod, "Id", "Description");
            return View();
        }

        // POST: Order/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OrderDate,ShippingDate,Comment,ShippingMethodsId,CustomersId,OrderStatusesId")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.DbSetOrder.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CustomersId = new SelectList(db.DbSetCustomer, "Id", "NickName", order.CustomersId);
            ViewBag.OrderStatusesId = new SelectList(db.DbSetOrderStatus, "Id", "Description", order.OrderStatusesId);
            ViewBag.ShippingMethodsId = new SelectList(db.DbSetShippingMethod, "Id", "Description", order.ShippingMethodsId);
            return View(order);
        }

        // GET: Order/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.DbSetOrder.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomersId = new SelectList(db.DbSetCustomer, "Id", "NickName", order.CustomersId);
            ViewBag.OrderStatusesId = new SelectList(db.DbSetOrderStatus, "Id", "Description", order.OrderStatusesId);
            ViewBag.ShippingMethodsId = new SelectList(db.DbSetShippingMethod, "Id", "Description", order.ShippingMethodsId);
            return View(order);
        }

        // POST: Order/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OrderDate,ShippingDate,Comment,ShippingMethodsId,CustomersId,OrderStatusesId")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomersId = new SelectList(db.DbSetCustomer, "Id", "NickName", order.CustomersId);
            ViewBag.OrderStatusesId = new SelectList(db.DbSetOrderStatus, "Id", "Description", order.OrderStatusesId);
            ViewBag.ShippingMethodsId = new SelectList(db.DbSetShippingMethod, "Id", "Description", order.ShippingMethodsId);
            return View(order);
        }

        // GET: Order/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.DbSetOrder.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.DbSetOrder.Find(id);
            db.DbSetOrder.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
