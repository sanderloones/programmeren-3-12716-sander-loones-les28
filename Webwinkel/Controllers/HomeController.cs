﻿using System;
using System.Web.Mvc;
using System.Data.Entity;

namespace Webwinkel.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Mikmak Webwinkel";
            return View();
        }

        public ActionResult AdminIndex()
        {
            ViewBag.Title = "Mikmak beheer";
            // We gaan ervan uit dat de database bestaat
            ViewBag.Feedback = "Database Webwinkel is al gemaakt.";
            // Kijk of de database al bestaat. Indien niet, maak de database.
            Webwinkel.Models.Dal Dal = new Models.Dal();

            Database.SetInitializer<Models.Dal>(new DropCreateDatabaseIfModelChanges<Models.Dal>());



            if (!Dal.Database.Exists())
            {
                try
                {
                    Dal.Database.Create();
                    ViewBag.Feedback = "Database Webwinkel is gemaakt!";
                }
                catch (Exception e)
                {
                    ViewBag.Feedback = e.Message;
                }
            }
     
            return View();
        }
    }
}