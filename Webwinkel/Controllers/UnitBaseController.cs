﻿using System.Web.Mvc;

namespace Webwinkel.Controllers
{
    public class UnitBaseController : Controller
    {
        public ActionResult Editing()
        {
            
            return View();
        }

        public ActionResult Inserting()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Insert(string UnitBaseCode, string UnitBaseName,
       string UnitBaseDescription)
        {
            Models.Dal dal = new Models.Dal();
            Models.UnitBase unitBase = new Models.UnitBase();
            unitBase.Code = UnitBaseCode;
            unitBase.Name = UnitBaseName;
            unitBase.Description = UnitBaseDescription;
            dal.DbSetUnitBase.Add(unitBase);
            dal.SaveChanges();
            return View("Inserting");
        }
        public ActionResult ReadingOne(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.UnitBase unitBase = new Models.UnitBase();
            unitBase.Id = id;
            unitBase = dal.DbSetUnitBase.Find(id);
            return View(unitBase);
        }
        public ActionResult InsertingCancel()
        {
            
            return View("Editing");
        }
        public ActionResult Cancel()
        {
            return View("Editing");
        }
        public ActionResult ReadingAll()
        {
            Models.Dal dal = new Models.Dal();
            return PartialView(dal.DbSetUnitBase);
        }
        public ActionResult Delete(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.UnitBase unitBase = new Models.UnitBase() { Id = id };
            dal.DbSetUnitBase.Attach(unitBase);
            dal.DbSetUnitBase.Remove(unitBase);
            dal.SaveChanges();
            return View("Editing");
        }
        public ActionResult Updating(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.UnitBase unitBase = new Models.UnitBase();
            unitBase.Id = id;
            unitBase = dal.DbSetUnitBase.Find(id);
            return View(unitBase);
        }

        [HttpPost]
        public ActionResult Update(string UnitBaseCode, string UnitBaseName,
        string UnitBaseDescription, string UnitBaseId)
        {
            Models.Dal dal = new Models.Dal();
            Models.UnitBase unitBase = new Models.UnitBase();
            unitBase.Id = System.Int32.Parse(UnitBaseId);
            unitBase.Code = UnitBaseCode;
            unitBase.Name = UnitBaseName;
            unitBase.Description = UnitBaseDescription;
            if (TryValidateModel(unitBase))
            {
                dal.DbSetUnitBase.Attach(unitBase);
                dal.Entry(unitBase).State = System.Data.Entity.EntityState.Modified;
                dal.SaveChanges();
            }
            return View("ReadingOne", unitBase);
        }

    }
}